import 'package:flutter/material.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      theme: ThemeData(
        primarySwatch: Colors.orange,
        scaffoldBackgroundColor: Colors.orange[100],
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text("Home - หน้าแรก"),
        ),
        body:
            // Center(
            //   child: Text(
            //     "Hello My App",
            //     style: TextStyle(
            //       fontSize: 35,
            //       fontWeight: FontWeight.bold,
            //     ),
            //   ),
            // ),

        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // FlutterLogo(size: 50),
            CircleAvatar(
              backgroundImage: AssetImage('images/Profile1.jpg'),
              radius: 50,
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Pakawat", style: TextStyle(fontSize: 25),),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text("Developer", style: TextStyle(fontSize: 25),),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
